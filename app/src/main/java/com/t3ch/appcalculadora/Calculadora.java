package com.t3ch.appcalculadora;

public class Calculadora {

    private float num1;
    private float num2;
    private float resultado;

    //-------------------------------------------------------------------

    //CONSTRUCTOR DE MI OBJETO
    public Calculadora()
    {
        this.setNum1(0.0f);
        this.setNum2(0.0f);
        this.setResultado(0.0f);
    }


//------------------------------------------------------------------------
    //SETTERS DE VALORES
    public void setNum1(float num1) {
        this.num1 = num1;
    }

    public void setNum2(float num2){
        this.num2 = num2;
    }

    public void setResultado(float resultado) {
        this.resultado = resultado;
    }
//------------------------------------------------------------------------

    //METODOS PARA OPERACIONES DE MI OBJETO

    public float suma()
    {
        this.resultado = this.num1+this.num2;
        return this.resultado;
    }

    public float resta()
    {
        this.resultado = this.num1-this.num2;
        return this.resultado;
    }

    public float multiplicacion()
    {
        this.resultado = this.num1*this.num2;
        return this.resultado;
    }

    public float division()
    {
        this.resultado = this.num1/this.num2;
        return this.resultado;
    }


}
