package com.t3ch.appcalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {

    private Calculadora calculadora;
    private TextView lblUser;
    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResultado;
    private Button btnSuma, btnResta, btnMulti, btnDiv, btnLimpiar, btnRegresar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        //Relaciono mis variables con mis objetos XML
        lblUser = (TextView)findViewById(R.id.lblUser);
        txtNum1 = (EditText)findViewById(R.id.lblNum1);
        txtNum2 = (EditText)findViewById(R.id.lblNum2);
        txtResultado = (EditText)findViewById(R.id.lblRes);
        btnSuma = (Button)findViewById(R.id.btnSuma);
        btnResta=(Button)findViewById(R.id.btnResta);
        btnMulti = (Button)findViewById(R.id.btnMulti);
        btnDiv = (Button)findViewById(R.id.btnDiv);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnRegresar = (Button)findViewById(R.id.btnRegresar);

        //Creo un objerto de la clase "Calculadora"
        calculadora = new Calculadora();

        //Recibo los datos de usuario de la primera pantalla
        Bundle dato = getIntent().getExtras();

        //Relaciono el dato del usuario a mi variable local y lomuestro en el XML
        String nombre = dato.getString("Usuario");
        lblUser.setText(nombre);

        //funcion para limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                txtNum1.setText("");
                txtNum2.setText("");
                txtResultado.setText("");
            }
        });

        //funcion para regresar
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //funcion para hacer la suma
        btnSuma.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (validacion())
                {
                    String res = String.valueOf(calculadora.suma());
                    txtResultado.setText(res);
                };

            }
        });

        //funcion para hacer la resta
        btnResta.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(validacion())
                {
                    String res = String.valueOf(calculadora.resta());
                    txtResultado.setText(res);
                }
            }
        });

        btnMulti.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(validacion())
                {
                    String res = String.valueOf(calculadora.multiplicacion());
                    txtResultado.setText(res);
                }
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(validacion())
                {
                    String res = String.valueOf(calculadora.division());
                    txtResultado.setText(res);
                }
            }
        });

    }

    //Funcion para validar que esten los campos llenos antes de cualquier operacion
    public Boolean validacion()
    {
        if (txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches(""))
        {
            Toast.makeText(CalculadoraActivity.this,"Por favor ingrese ambos numeros",Toast.LENGTH_SHORT).show();
            return false;
        }
        else
        {   //le agrego a mi objeto los numeros capturados
            calculadora.setNum1(Float.parseFloat(txtNum1.getText().toString()));
            calculadora.setNum2(Float.parseFloat(txtNum2.getText().toString()));
            return true;
        }
    }
}