package com.t3ch.appcalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{

    private TextView lblUsuario;
    private TextView lblContraseña;
    private Button btnIngresar;
    private Button btnSalir;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblUsuario = (TextView) findViewById(R.id.txtUsuario);
        lblContraseña = (TextView) findViewById(R.id.txtContraseña);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        //Dentro del oncreate hago mi validacion de que se llennenn ambos datos en caso de click en boton ingresar
        btnIngresar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (lblUsuario.getText().toString().trim().equalsIgnoreCase("") || lblContraseña.getText().toString().trim().equalsIgnoreCase(""))
                {
                    Toast.makeText(MainActivity.this, "Favor de introducir los datos correctos", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Intent lacalc = new Intent(MainActivity.this, CalculadoraActivity.class);
                    lacalc.putExtra("Usuario", lblUsuario.getText().toString());
                    startActivity(lacalc);
                }
            }

        }
        );
        //codifico mi funcion de salir dentro del OnCreate
        btnSalir.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        }
        );

    }


}
